package models

trait Identifiable[I] {
  def id: I
}

case class Project(id: String, name: String) extends Identifiable[String]

case class User(id: String, givenName: String, familyName: String, email: String) extends Identifiable[String]
case class UserCredentials(userId: String, password: String)
case class Group(id: String, name: String) extends Identifiable[String]
case class Role(id: String, name: String) extends Identifiable[String]
case class UserGroup(userId: String, groupId: String)
case class UserRole(userId: String, roleId: String)
case class GroupRole(groupId: String, roleId: String)
case class Permission(id: Long, roleId: String, target: String, action: String, isGranted: Boolean) extends Identifiable[Long]
case class Identity(token: String, user: User, roles: Seq[Role], permissions: Seq[Permission])
case class Avatar(id: String, mimeType: String, imageData: Array[Byte]) extends Identifiable[String]

case class Task(id: String, description: String, creatorId: String, assigneeId: Option[String], assignedGropuId: Option[String], projectId: String, isComplete: Boolean) extends Identifiable[String]
case class TaskDependency(taskId: String, requiredTaskId: String)
case class TaskTree(task: Option[Task], requiredTasks: List[TaskTree])
case class Comment(id: Option[Long], taskId: String, creatorId: String, value: String) extends Identifiable[Option[Long]]
