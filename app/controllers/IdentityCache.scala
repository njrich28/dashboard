package controllers

import javax.inject.Inject
import play.api.cache.CacheApi
import com.google.inject.ImplementedBy
import models.Identity

@ImplementedBy(classOf[DefaultIdentityCache])
trait IdentityCache {
  def set(token: String, identity: Identity): Unit
  def get(token: String): Option[Identity]
  def remove(token: String): Unit
}

class DefaultIdentityCache @Inject() (cacheApi: CacheApi) extends IdentityCache {
  def set(token: String, identity: Identity): Unit = cacheApi.set(token, identity)
  def get(token: String): Option[Identity] = cacheApi.get[Identity](token)
  def remove(token: String): Unit = cacheApi.remove(token)
}