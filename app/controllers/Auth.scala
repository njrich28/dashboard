package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future
import javax.inject.Inject
import daos.UserDao
import models.Identity
import io.github.nremond.SecureHash

class Auth @Inject() (protected val identityCache: IdentityCache, userDao: UserDao) extends Controller with Security {
  /** Used for obtaining the email and password from the HTTP login request */
  case class LoginCredentials(username: String, password: String)

  /** JSON reader for [[LoginCredentials]]. */
  implicit val loginCredentialsFromJson = (
    (__ \ "username").read[String](minLength[String](4)) ~
    (__ \ "password").read[String](minLength[String](2))
  )((username, password) => LoginCredentials(username, password))
  
  def authenticate = Action.async(parse.json) { implicit request =>
    request.body.validate[LoginCredentials].fold(
      errors => {
        Future.successful(BadRequest(Json.obj("status" -> "ERROR", "message" -> errors.flatMap{ case (path, validationErrors) => validationErrors.map(error => s"$path => ${error.message}")}.mkString(", "))))
      },
      credentials => {
        userDao.credentialsById(credentials.username).map { maybeUserCredentials =>
          maybeUserCredentials.fold {
            BadRequest(Json.obj("status" -> "ERROR", "message" -> "User not registered"))
          } { userCredentials =>
            if (SecureHash.validatePassword(credentials.password, userCredentials.password)) {
              val token = java.util.UUID.randomUUID.toString
              val roles = userDao.allRolesByUser(userCredentials.userId)
              val permissions = userDao.allPermissionsByUser(userCredentials.userId)
              val user = userDao.byId(userCredentials.userId)
              val futureUserPermissions = for {
                r <- roles
                p <- permissions
                u <- user
              } yield Identity(token, u.get, r, p)
              futureUserPermissions.foreach { userPermissions =>
                identityCache.set(token, userPermissions)            
              }
  
              Ok(Json.obj("status" -> "OK", "token" -> token))
                .withCookies(Cookie(AuthTokenCookieKey, token, None, httpOnly = false))
            } else {
              BadRequest(Json.obj("status" -> "ERROR", "message" -> "User not registered"))            
            }
          }
        }
      }
    )
  }
  
  def logout = AuthenticatedAction { req =>
    identityCache.remove(req.identity.token)
    Ok.discardingCookies(DiscardingCookie(name = AuthTokenCookieKey))
  }
}