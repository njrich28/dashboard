package controllers

import play.api.mvc.{Action, Controller}
import javax.inject.Inject
import daos.GroupDao
import models.Group
import play.api.libs.json.Json
import scala.concurrent.ExecutionContext

class GroupsApi @Inject() (protected val identityCache: IdentityCache, groupDao: GroupDao)(implicit ec: ExecutionContext) extends Controller with Security {
  implicit val groupFormat = Json.format[Group]

  def index = Action.async {
    groupDao.all.map { groups =>
      Ok(Json.toJson(groups))
    }
  }
  
  def view(groupId: String) = AuthenticatedAction.async {
    groupDao.byId(groupId).map { groupOpt =>
      groupOpt.map { group =>
        Ok(Json.toJson(group))
      }.getOrElse(NotFound)
    }
  }
  
  def update(groupId: String) = AuthenticatedAction.async(parse.json[Group]) { req =>
    val group = req.body
    groupDao.put(group).map{ result =>
      if (result) {
        Ok
      } else {
        BadRequest
      }
    }
  }

}