package controllers

import play.api.mvc.{ Action, Controller, Cookie }
import scala.concurrent.Future
import javax.inject.Inject
import daos.UserDao
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import models.{ Group, Role, User, UserCredentials, Permission }
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models.Identity
import play.api.mvc.DiscardingCookie
import io.github.nremond.SecureHash

class UsersApi @Inject() (protected val identityCache: IdentityCache, userDao: UserDao) extends Controller with Security {
  implicit val userFormat = Json.format[User]
  implicit val roleFormat = Json.format[Role]
  implicit val groupFormat = Json.format[Group]
  implicit val permmissionFormat = Json.format[Permission]
  implicit val credentialsFormat = Json.format[UserCredentials]
  
  def index = AuthenticatedAction.async {
    userDao.all.map { users =>
      Ok(Json.toJson(users))
    }
  }
  
  def view(userId: String) = AuthenticatedAction.async {
    userDao.byId(userId).map { userOpt =>
      userOpt.map { user =>
        Ok(Json.toJson(user))
      }.getOrElse(NotFound)
    }
  }
  
  def authenticated = AuthenticatedAction { req =>
    Ok(Json.toJson(req.identity.user))
  }
  
  def update(userId: String) = AuthenticatedAction.async(parse.json[User]) { req =>
    val user = req.body
    userDao.put(user).map{ result =>
      if (result) {
        Ok
      } else {
        BadRequest
      }
    }
  }
  
  def updateCredentials(userId: String) = AuthenticatedAction.async(parse.json[UserCredentials]) { req =>
    val userCredentials = req.body
//    if (userCredentials.userId == req.identity.user.id) {
      val hashedUserCredentials = userCredentials.copy(password = SecureHash.createHash(userCredentials.password))
      userDao.putCredentials(hashedUserCredentials).map { result =>
        if (result) {
          Ok
        } else {
          BadRequest
        }
      }
//    } else {
//      Future.successful(Forbidden)
//    }
  }
  
  def groups(userId: String) = AuthenticatedAction.async {
    userDao.groupsByUser(userId).map { groups =>
      Ok(Json.toJson(groups))
    }
  }
  
  def roles(userId: String) = AuthenticatedAction.async {
    userDao.allRolesByUser(userId).map { roles =>
      Ok(Json.toJson(roles))
    }
  }
  
  def permissions(userId: String) = AuthenticatedAction.async {
    userDao.allPermissionsByUser(userId).map { permissions =>
      Ok(Json.toJson(permissions))
    }
  }
}