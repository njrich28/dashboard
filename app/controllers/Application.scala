package controllers

import play.api._
import play.api.mvc._

class Application extends Controller {
  
  def index = Action {
    Ok(views.html.index(Nil))
  }
  
  def signin = Action {
    Ok(views.html.signin())
  }

  def tasktree(projectId: String) = Action {
    Ok(views.html.tasktree(projectId))
  }

}
