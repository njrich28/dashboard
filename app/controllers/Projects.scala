package controllers

import play.api.mvc.{ Action, Controller }
import play.api.libs.json.Json
import models.Project
import javax.inject.Inject
import daos.{ProjectDao, TaskDao}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future

class Projects @Inject() (protected val identityCache: IdentityCache, projectDao: ProjectDao, taskDao: TaskDao) extends Controller with Security {
  
  implicit val projectFormat = Json.format[Project]
  
  case class BodyProject(name: String, templateProjectId: Option[String])
  implicit val bodyProjectFormat = Json.format[BodyProject]
  
  def index = Action.async {
    projectDao.all map { projects =>
      Ok(Json.toJson(projects))
    }
  }
  
  def view(id: String) = Action.async {
    projectDao.byId(id) map { projectOpt =>
      projectOpt.map { p =>
        Ok(Json.toJson(p))
      }.getOrElse(NotFound) 
    }
  }
  
  def create(id: String) = AuthenticatedAction.async(parse.json[BodyProject]) { req =>
    val sourceProjectId = req.body.templateProjectId
    val r = for {
      projectCreated <- projectDao.create(Project(id, req.body.name))
      tasksCopiedCount <- sourceProjectId.map(taskDao.copyTasks(_, id)).getOrElse(Future.successful(0))
    } yield (projectCreated, tasksCopiedCount)
    r map { case (projectCreated, tasksCopiedCount) => 
      if (projectCreated) {
        Created(Json.obj("tasksCopied" -> tasksCopiedCount))
      } else {
        BadRequest
      }
    }
  }
}