package controllers

import play.api.mvc.{ Action, Controller }
import scala.concurrent.Future
import javax.inject.Inject
import daos.TaskDao
import play.api.libs.json.Json
import models.{Task, TaskTree}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Tasks @Inject() (protected val identityCache: IdentityCache, taskDao: TaskDao) extends Controller with Security {
  
  case class BodyTaskTree(description: String, isComplete: Option[Boolean], assigneeId: Option[String], assignedGroupId: Option[String], requiredTaskIds: Option[List[String]], requiredByTaskIds: Option[List[String]])

  implicit val taskFormat = Json.format[Task]
  implicit val taskTreeFormat = Json.format[TaskTree]
  implicit val bodyTaskFormat = Json.format[BodyTaskTree]
  
  def tree(projectId: String) = Action.async {
    taskDao.taskTree(projectId) map { taskTree =>
      Ok(Json.toJson(taskTree.requiredTasks))
    }
  }
  
  def index(projectId: String) = Action.async {
    taskDao.byProject(projectId) map { tasks =>
      Ok(Json.toJson(tasks))
    }
  }
  
//  def index = Action.async {
//    
//  }
  
  def view(projectId:String, taskId: String) = Action.async {
    taskDao.byId(taskId) map { taskOpt =>
      taskOpt.map { t =>
        Ok(Json.toJson(t))
      }.getOrElse(NotFound)
    }
  }
  
  def update(projectId: String, taskId: String) = AuthenticatedAction.async(parse.json[BodyTaskTree]) { req =>
    val b = req.body
    val task = Task(taskId, b.description, req.identity.user.id, b.assigneeId, b.assignedGroupId, projectId, b.isComplete.getOrElse(false))
    taskDao.create(task, b.requiredTaskIds.getOrElse(List.empty), b.requiredByTaskIds.getOrElse(List.empty)) map { result =>
      if (result) Created else BadRequest
    }
  }
  
  def assignee(userId: String) = Action.async {
    taskDao.byAssignee(userId) map { tasks =>
      Ok(Json.toJson(tasks))
    }
  }
}