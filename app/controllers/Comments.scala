package controllers

import play.api.mvc._
import javax.inject.Inject
import daos.CommentDao
import play.api.libs.json.Json
import models.Comment
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Comments @Inject() (protected val identityCache: IdentityCache, commentDao: CommentDao) extends Controller with Security {
  case class BodyComment(value: String)
  implicit val commentFormat = Json.format[Comment]
  implicit val bodyCommentFormat = Json.format[BodyComment]
  
  def index(projectId: String, taskId: String) = Action.async {
    commentDao.byTask(taskId).map { comments =>
      Ok(Json.toJson(comments))
    }
  }

  def post(projectId: String, taskId: String) = AuthenticatedAction.async(parse.json[BodyComment]) { req =>
    val bodyComment = req.body
    val user = req.identity.user
    val comment = Comment(None, taskId, user.id, bodyComment.value)
    commentDao.put(comment).map { result =>
      if (result) {
        Created
      } else {
        BadRequest
      }
    }
  }
}