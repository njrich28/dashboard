package controllers

import play.api.mvc._
import play.api.mvc.Security._
import models.Identity
import scala.concurrent.Future
import play.api.libs.json._

class AuthenticatedIdentityRequest[A](identity: Identity, request: Request[A]) extends AuthenticatedRequest[A, Identity](identity, request) {
  def identity = user
}
class IdentityAwareRequest[A](identity: Option[Identity], request: Request[A]) extends WrappedRequest[A](request)

trait Security { self: Controller =>
  
  protected val identityCache: IdentityCache
  
  val AuthTokenHeader = "X-XSRF-TOKEN"
  val AuthTokenCookieKey = "XSRF-TOKEN"
  val AuthTokenUrlKey = "auth"
  
  object AuthenticatedAction extends ActionBuilder[AuthenticatedIdentityRequest] {
    def invokeBlock[A](request: Request[A], block: (AuthenticatedIdentityRequest[A]) => Future[Result]) = {
      AuthenticatedBuilder(req => getIdentityFromRequest(req), _ => Unauthorized).authenticate(request, { authRequest: AuthenticatedRequest[A, Identity] =>
        block(new AuthenticatedIdentityRequest[A](authRequest.user, request))
      })
    }
  }
  
  object IdentityAwareAction extends ActionBuilder[IdentityAwareRequest] with ActionTransformer[Request, IdentityAwareRequest] {
    def transform[A](request: Request[A]) = Future.successful {
      new IdentityAwareRequest[A](getIdentityFromRequest(request), request)
    }
  }
  
  private def getIdentityFromRequest[A](req: RequestHeader): Option[Identity] = {
    for {
      xsrfTokenCookie <- req.cookies.get(AuthTokenCookieKey)
      xsrfToken <- req.headers.get(AuthTokenHeader).orElse(req.getQueryString(AuthTokenUrlKey)) if xsrfTokenCookie.value == xsrfToken
      identity <- identityCache.get(xsrfToken)
    } yield identity
  }
}