package controllers

import play.api.mvc._
import daos.AvatarDao
import javax.inject.Inject
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future
import models.Avatar
import java.io.FileInputStream
import com.google.common.io.ByteStreams

class Avatars @Inject() (protected val identityCache: IdentityCache, avatarDao: AvatarDao) extends Controller with Security {
  def get(avatarId: String) = Action.async {
    avatarDao.byId(avatarId).map { maybeAvatar =>
      maybeAvatar.map { avatar =>
        Ok(avatar.imageData).as(avatar.mimeType)
      }.getOrElse(NotFound)
    }
  }
  
  def put(avatarId: String) = Action.async(parse.multipartFormData) { req =>
    val maybeAvatar = req.body.file("avatarFile").map { avatarFile =>
      val mimeType = avatarFile.contentType
      val byteArray = ByteStreams.toByteArray(new FileInputStream(avatarFile.ref.file))
      Avatar(avatarId, mimeType.getOrElse("application/octet-stream"), byteArray)
    }
    maybeAvatar.map { avatar =>
      avatarDao.put(avatar).map { _ =>
        Ok
      }
    }.getOrElse(Future.successful(BadRequest))    
  }
}