package daos

import com.google.inject.ImplementedBy
import javax.inject.Inject
import scala.concurrent.Future
import models.{Task, TaskDependency, TaskTree}
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[SlickTaskDao])
trait TaskDao {
  def all: Future[Seq[Task]]
  def byId(id: String): Future[Option[Task]]
  def create(task: Task, requiredTaskIds: Seq[String], requiredByTaskIds: Seq[String]): Future[Boolean]
  def withoutDependency: Future[Seq[Task]]
  def taskTree(projectId: String): Future[TaskTree]
  def byProject(projectId: String): Future[Seq[Task]]
  def countByProject(projectId: String): Future[(Int, Int)]
  def byAssignee(userId: String): Future[Seq[Task]]
  def copyTasks(sourceProjectId: String, destProjectId: String): Future[Int]
}

class SlickTaskDao @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends TaskDao with GenericDao[Task] with Mappings with All[Task] with ByStringId[Task] {
  import driver.api._
  
  val tableQuery = tasks
  
  case class LiftedTask(id: Rep[String], description: Rep[String], creatorId: Rep[String], assigneeId: Rep[Option[String]], assignedGropuId: Rep[Option[String]], projectId: Rep[String], isComplete: Rep[Boolean])
  implicit object TaskShape extends CaseClassShape(LiftedTask.tupled, Task.tupled)
  
  def byProject(projectId: String): Future[Seq[Task]] = db.run(tasks.filter(_.projectId === projectId).result)
  
  def countByProject(projectId: String): Future[(Int, Int)] = {
    val res = db.run(tasks.filter(_.projectId === projectId).groupBy(_.isComplete).map { case (isComplete, ts) => (isComplete, ts.length)}.result)
    
    Future.successful((10,100))
  }
  
  def byAssignee(userId: String): Future[Seq[Task]] = db.run(tasks.filter(_.assigneeId === userId).result)
  
  def copyTasks(sourceProjectId: String, destProjectId: String): Future[Int] = {
    db.run(tasks forceInsertQuery (tasks.filter(_.projectId === sourceProjectId).map { t => 
      LiftedTask(t.id, t.description, t.creatorId, None, t.assignedGroupId, destProjectId, false)
    }))
  }

  def create(task: Task, requiredTaskIds: Seq[String], requiredByTaskIds: Seq[String]): Future[Boolean] = {
    val newTaskDependencies = requiredTaskIds.map(TaskDependency(task.id, _)) ++ requiredByTaskIds.map(TaskDependency(_, task.id))
    
    db.run(tasks insertOrUpdate task).flatMap { result =>
      if (result == 1) {
        if (newTaskDependencies.isEmpty) {
          Future.successful(true)
        } else {
          db.run(taskDependencies ++= newTaskDependencies).map(_.map(_ != 0).getOrElse(false))
        }
      } else Future.successful(false)
    }
  } 
  
  def withoutDependency: Future[Seq[Task]] = {
    val q = for {
      (t, td) <- tasks joinLeft taskDependencies on (_.id === _.taskId) if td isEmpty
    } yield t
    db.run(q.result)
  }
  
  def requiredFor(projectId: String, taskIdOpt: Option[String]): Future[List[Task]] = {
    def requiredForQuery(taskId: String) = {
      for {
        (t, td) <- tasks join taskDependencies on (_.id === _.requiredTaskId) if td.taskId === taskId && t.projectId === projectId
      } yield t 
    }
    
    def requiredForNoneQuery = {
      // This join query would be more efficient but doesn't work due to https://github.com/slick/slick/issues/1156
      //
      // for {
      //   (t, td) <- tasks joinLeft taskDependencies on (_.id === _.requiredTaskId) if td.isEmpty && t.projectId === projectId
      // } yield t
      //
      // Use less efficient subquery:
      tasks.filter(_.projectId === projectId).filterNot(_.id in taskDependencies.map(_.requiredTaskId))
    }
    
    db.run((taskIdOpt map requiredForQuery getOrElse requiredForNoneQuery).result).map(_.toList)
  }
  
  def taskTree(projectId: String): Future[TaskTree] = {
    def taskTreeRecursive(taskOpt: Option[Task]): Future[TaskTree] = {
      requiredFor(projectId, taskOpt.map(_.id)).flatMap { requiredTasks =>
        Future.traverse(requiredTasks) { t =>
          taskTreeRecursive(Some(t))
        }.map { requiredTaskTrees =>
          TaskTree(taskOpt, requiredTaskTrees)
        }
      }
    }
    taskTreeRecursive(None)
  }
}