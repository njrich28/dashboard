package daos

import com.google.inject.ImplementedBy
import javax.inject.Inject
import scala.concurrent.Future
import models.{User, Group, Role, Permission, UserCredentials}
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[SlickGroupDao])
trait GroupDao {
  def all: Future[Seq[Group]]
  def byId(groupId: String): Future[Option[Group]]
  def put(group: Group): Future[Boolean]
}

class SlickGroupDao @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends GroupDao with GenericDao[Group] with Mappings with All[Group] with ByStringId[Group] {
  import driver.api._
  val tableQuery = groups
  def put(group: Group): Future[Boolean] = db.run(groups insertOrUpdate group).map(_ == 1)
}
