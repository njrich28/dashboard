package daos

import com.google.inject.ImplementedBy
import javax.inject.Inject
import scala.concurrent.Future
import models.Project
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[SlickProjectDao])
trait ProjectDao {
  def all: Future[Seq[Project]]
  def byId(id: String): Future[Option[Project]]
  def create(project: Project): Future[Boolean] 
}

class SlickProjectDao @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends ProjectDao with GenericDao[Project] with Mappings with All[Project] with ByStringId[Project] {
  import driver.api._
  val tableQuery = projects
  def create(project: Project): Future[Boolean] = db.run(projects insertOrUpdate project).map(_ == 1)
}

