package daos

import play.api.db.slick.HasDatabaseConfigProvider
import slick.driver.JdbcProfile
import play.api.db.slick.DatabaseConfigProvider
import models._
import scala.concurrent.Future

/**
 * Mixin required by the implementation mixins to specify the table to be queried
 */
trait GenericDao[E] extends HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._
  val tableQuery: TableQuery[_ <: Table[E]]
}

/**
 * Mixin to provide implementation of all query returning all rows from a table
 */
trait All[E <: Identifiable[_]] extends HasDatabaseConfigProvider[JdbcProfile] { self: GenericDao[E] =>
  import driver.api._
  def all: Future[Seq[E]] = db.run(cast(tableQuery).result)
  // workaround for restrictive invariant type
  private def cast[E](tq: TableQuery[_ <: Table[E]]) = tq.asInstanceOf[TableQuery[Table[E]]]
}

/**
 * Mixin to provide implementation of byId which returns option row by String primary key
 */
trait ByStringId[E <: Identifiable[String]] extends HasDatabaseConfigProvider[JdbcProfile] { self: GenericDao[E] =>
  import driver.api._
  def byId(id: String): Future[Option[E]] = db.run(castWithId(tableQuery).filter(_.id === id).result.headOption)
  // workaround for restrictive invariant type
  private def castWithId[E](tq: TableQuery[_ <: Table[E]]) = tq.asInstanceOf[TableQuery[Table[E] with IdColumn[String]]]
}

