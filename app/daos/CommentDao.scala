package daos

import com.google.inject.ImplementedBy
import javax.inject.Inject
import scala.concurrent.Future
import models.Comment
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[SlickCommentDao])
trait CommentDao {
  def byTask(taskId: String): Future[Seq[Comment]]
  def put(comment: Comment): Future[Boolean]
}

class SlickCommentDao @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends CommentDao with Mappings {
  import driver.api._
  
  def byTask(taskId: String) = db.run(comments.filter(_.taskId === taskId).result)
  
  def put(comment: Comment) = db.run(comments += comment).map(_ == 1)
}