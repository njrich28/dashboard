package daos

import com.google.inject.ImplementedBy
import javax.inject.Inject
import scala.concurrent.Future
import models.{User, Group, Role, Permission, UserCredentials}
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[SlickUserDao])
trait UserDao {
  def byId(userId: String): Future[Option[User]]
  def all: Future[Seq[User]]
  def put(user: User): Future[Boolean]
  def groupsByUser(userId: String): Future[Seq[Group]]
  def allRolesByUser(userId: String): Future[Seq[Role]]
  def allPermissionsByUser(userId: String): Future[Seq[Permission]]
  def credentialsById(userId: String): Future[Option[UserCredentials]]
  def putCredentials(userCredentials: UserCredentials): Future[Boolean]
}

class SlickUserDao @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends UserDao with GenericDao[User] with Mappings with All[User] with ByStringId[User] {
  import driver.api._

  val tableQuery = users
  
  def put(user: User): Future[Boolean] = db.run(users insertOrUpdate user).map(_ == 1)
  
  def credentialsById(userId: String): Future[Option[UserCredentials]] = db.run(userCredentials.filter(_.userId === userId).result.headOption)

  def putCredentials(newUserCredentials: UserCredentials): Future[Boolean] = db.run(userCredentials insertOrUpdate newUserCredentials).map(_ == 1)

  def groupsByUser(userId: String): Future[Seq[Group]] = {
    val q = for {
      (ug, g) <- userGroups join groups on (_.groupId === _.id) if ug.userId === userId
    } yield g
    db.run(q.result)
  }
  
  def rolesByUser(userId: String): Future[Seq[Role]] = {
    val q = for {
      (ur, r) <- userRoles join roles on (_.roleId === _.id) if ur.userId === userId
    } yield r
    db.run(q.result)
  }
  
  def rolesByGroup(groupId: String): Future[Seq[Role]] = {
    val q = for {
      (gr, r) <- groupRoles join roles on (_.roleId === _.id) if gr.groupId === groupId
    } yield r
    db.run(q.result)
  }
  
  def groupRolesByUser(userId: String): Future[Seq[Role]] = {
    val q = for {
      ug <- userGroups if ug.userId === userId
      gr <- groupRoles if ug.groupId === gr.groupId
      r <- roles if gr.roleId === r.id
    } yield r
    db.run(q.result)
  }
  
  def allRolesByUser(userId: String): Future[Seq[Role]] = {
    val directRoles = rolesByUser(userId)
    val groupRoles = groupRolesByUser(userId)
    for {
      dr <- directRoles
      gr <- groupRoles
    } yield (dr ++ gr).distinct
  }
  
  def groupPermissionsByUser(userId: String): Future[Seq[Permission]] = {
    val q = for {
      ug <- userGroups if ug.userId === userId
      gr <- groupRoles if ug.groupId === gr.groupId
      p <- permissions if p.roleId === gr.roleId
    } yield p
    db.run(q.result)
  }
  
  def permissionsByUser(userId: String): Future[Seq[Permission]] = {
    val q = for {
      ur <- userRoles if ur.userId === userId
      p <- permissions if p.roleId === ur.roleId
    } yield p
    db.run(q.result)
  }
  
  def allPermissionsByUser(userId: String): Future[Seq[Permission]] = {
    val directPermissions = permissionsByUser(userId)
    val groupPermissions = groupPermissionsByUser(userId)
    for {
      dp <- directPermissions
      gp <- groupPermissions
    } yield (dp ++ gp).distinct
  }
}
