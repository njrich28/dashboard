package daos

import javax.inject.Inject
import play.api.db.slick.HasDatabaseConfigProvider
import slick.driver.JdbcProfile
import play.api.db.slick.DatabaseConfigProvider
import models._
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.annotation.tailrec
import com.google.inject.ImplementedBy
import slick.lifted.Rep

trait IdColumn[I] {
  def id: Rep[I]
}
  
trait Mappings extends HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._
  
  val projects = TableQuery[ProjectsTable]
  val users = TableQuery[UsersTable]
  val userCredentials = TableQuery[UserCredentialsTable]
  val groups = TableQuery[GroupsTable]
  val roles = TableQuery[RolesTable]
  val userGroups = TableQuery[UserGroupsTable]
  val userRoles = TableQuery[UserRolesTable]
  val groupRoles = TableQuery[GroupRolesTable]
  val permissions = TableQuery[PermissionsTable]
  val tasks = TableQuery[TasksTable]
  val taskDependencies = TableQuery[TaskDependenciesTable]
  val avatars = TableQuery[AvatarsTable]
  val comments = TableQuery[CommentsTable]
  
  trait StringIdColumn extends IdColumn[String] { self: Table[_] =>
    def id = column[String]("id", O.PrimaryKey)
  }
  
  trait LongIdColumn extends IdColumn[Long] { self: Table[_] =>
    def id = column[Long]("id", O.PrimaryKey)
  }
  
  class ProjectsTable(tag: Tag) extends Table[Project](tag, "project") with StringIdColumn {
    def name = column[String]("name")
    
    def * = (id, name) <> (Project.tupled, Project.unapply)
  }
  
  class UsersTable(tag: Tag) extends Table[User](tag, "user") with StringIdColumn {
    def givenName = column[String]("givenName")
    def familyName = column[String]("familyName")
    def email = column[String]("email")
    
    def * = (id, givenName, familyName, email) <> (User.tupled, User.unapply)
  }
  
  class UserCredentialsTable(tag: Tag) extends Table[UserCredentials](tag, "usercredentials") {
    def userId = column[String]("user_id", O.PrimaryKey)
    def password = column[String]("password")
    def * = (userId, password) <> (UserCredentials.tupled, UserCredentials.unapply)
  }
  
  class GroupsTable(tag: Tag) extends Table[Group](tag, "group") with StringIdColumn {
    def name = column[String]("name")
    
    def * = (id, name) <> (Group.tupled, Group.unapply)
  }
  
  class RolesTable(tag: Tag) extends Table[Role](tag, "role") with StringIdColumn {
    def name = column[String]("name")
    
    def * = (id, name) <> (Role.tupled, Role.unapply)
  }
  
  class UserGroupsTable(tag: Tag) extends Table[UserGroup](tag, "usergroup") {
    def userId = column[String]("user_id")
    def groupId = column[String]("group_id")
    
    def * = (userId, groupId) <> (UserGroup.tupled, UserGroup.unapply)
  }
  
  class UserRolesTable(tag: Tag) extends Table[UserRole](tag, "userrole") {
    def userId = column[String]("user_id")
    def roleId = column[String]("role_id")
    
    def * = (userId, roleId) <> (UserRole.tupled, UserRole.unapply)
  }
  
  class GroupRolesTable(tag: Tag) extends Table[GroupRole](tag, "grouprole") {
    def groupId = column[String]("group_id")
    def roleId = column[String]("role_id")
    
    def * = (groupId, roleId) <> (GroupRole.tupled, GroupRole.unapply)
  }
  
  class PermissionsTable(tag: Tag) extends Table[Permission](tag, "permission") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def roleId = column[String]("role_id")
    def target = column[String]("target")
    def action = column[String]("action")
    def isGranted = column[Boolean]("granted")
    
    def * = (id, roleId, target, action, isGranted) <> (Permission.tupled, Permission.unapply)
  }
  
  class TasksTable(tag: Tag) extends Table[Task](tag, "task") with StringIdColumn {
    def description = column[String]("description")
    def creatorId = column[String]("creator_id")
    def assigneeId = column[Option[String]]("assignee_id")
    def assignedGroupId = column[Option[String]]("assignedGroup_id")
    def projectId = column[String]("project_id")
    def isComplete = column[Boolean]("complete")
    
    def project = foreignKey("projectfk", projectId, projects)(_.id)
    
    def * = (id, description, creatorId, assigneeId, assignedGroupId, projectId, isComplete) <> (Task.tupled, Task.unapply)
  }
  
  class TaskDependenciesTable(tag: Tag) extends Table[TaskDependency](tag, "taskdependency") {
    def taskId = column[String]("task_id")
    def requiredTaskId = column[String]("requiredTask_id")
    
    def pk = primaryKey("tdpk", (taskId, requiredTaskId))
    
    def task = foreignKey("taskfk", taskId, tasks)(_.id)
    def requiredTask = foreignKey("requiredtaskfk", requiredTaskId, tasks)(_.id)
    
    def * = (taskId, requiredTaskId) <> (TaskDependency.tupled, TaskDependency.unapply)
  }
  
  class AvatarsTable(tag: Tag) extends Table[Avatar](tag, "avatar") with StringIdColumn {
    def mimeType = column[String]("mimeType")
    def imageData = column[Array[Byte]]("imageData")
    
    def * = (id, mimeType, imageData) <> (Avatar.tupled, Avatar.unapply)
  }
  
  class CommentsTable(tag: Tag) extends Table[Comment](tag, "comment") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def taskId = column[String]("task_id")
    def creatorId = column[String]("creator_id")
    def value = column[String]("value")
    
    def * = (id, taskId, creatorId, value) <> (Comment.tupled, Comment.unapply)
  }
  
}
