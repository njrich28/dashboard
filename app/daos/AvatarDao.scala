package daos

import com.google.inject.ImplementedBy
import javax.inject.Inject
import scala.concurrent.Future
import models.Avatar
import play.api.db.slick.DatabaseConfigProvider
import scala.concurrent.ExecutionContext

@ImplementedBy(classOf[SlickAvatarDao])
trait AvatarDao {
  def byId(id: String): Future[Option[Avatar]]
  def put(avatar: Avatar): Future[Boolean]
}

class SlickAvatarDao @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends AvatarDao with GenericDao[Avatar] with Mappings with ByStringId[Avatar] {
  import driver.api._  
  val tableQuery = avatars
  
  def put(avatar: Avatar): Future[Boolean] = db.run(avatars insertOrUpdate avatar).map(_ == 1)
}

