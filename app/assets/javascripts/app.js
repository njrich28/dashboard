
var dashboard = angular.module('dashboard', ['ngResource', 'ngRoute', 'ngCookies', 'angularFileUpload']);

dashboard.controller('HomeCtrl', ['$scope', '$location', 'Auth', 'UserTasks', function($scope, $location, Auth, UserTasks) {
	if (!Auth.getUser()) {
		$location.path('/login');
	} else {
		$scope.tasks = UserTasks.query({userId: Auth.getUser().id});
	}
}]);

dashboard.controller('ProjectsCtrl', ['$scope', 'Projects', function($scope, Projects) {
	$scope.projects = Projects.query();
}]);

dashboard.controller('ProjectCtrl', ['$scope', '$routeParams', '$location', 'Projects', 'Tasks', function($scope, $routeParams, $location, Projects, Tasks) {
	$scope.tasks = undefined;
	$scope.project = undefined;
	$scope.error = undefined;
	$scope.task = {	projectId: $routeParams.projectId };
	
	$scope.createTask = function() {
		Tasks.save({}, $scope.task, function(val, responseHeaders) {
			$scope.task.id = undefined;
			$scope.task.description = undefined;
			Projects.get({projectId: $routeParams.projectId}, function(project, responseHeaders) {
				$scope.project = project;
				$scope.tasks = Tasks.query({projectId: project.id});
			}, function(errorResult) {
				$scope.error = errorResult;
			});			
		});
	};
	
	$scope.toggle = function(task) {
		task.isComplete = !task.isComplete;
		task.$save();
	};
	
	$scope.save = function() {
		$scope.project.$save(null, function(data, responseHeaders) {
			$location.path('/projects/' + $scope.project.id);
		});
	};
	
	Projects.get({projectId: $routeParams.projectId}, function(project, responseHeaders) {
		$scope.project = project;
		$scope.pageTitle = project.name;
		$scope.tasks = Tasks.query({projectId: project.id});
	}, function(errorResult) {
		$scope.error = errorResult;
	});
}]);

dashboard.controller('ProjectCreateCtrl', ['$scope', '$location', 'Projects', function($scope, $location, Projects) {
	$scope.project = {};
	$scope.pageTitle = 'Create project';
	$scope.error = undefined;
	$scope.templates = Projects.query();
	
	$scope.save = function() {
		Projects.save(null, $scope.project, function(data, responseHeaders) {
			$location.path('/projects/' + $scope.project.id);
		}, function(errorResult) {
			$scope.error = errorResult;
		});
	};
}]);

dashboard.controller('TaskCtrl', ['$scope', '$routeParams', 'Tasks', 'Comments', function($scope, $routeParams, Tasks, Comments) {
	$scope.task = undefined;
	$scope.error = undefined;
	$scope.comments = Comments.query({projectId: $routeParams.projectId, taskId: $routeParams.taskId});
	$scope.comment = {};
	
	Tasks.get({projectId: $routeParams.projectId, taskId: $routeParams.taskId}, function(task, responseHeaders) {
		$scope.task = task;
	}, function(errorResult) {
		$scope.error = errorResult;
	});
	
	$scope.postComment = function() {
		Comments.save({projectId: $routeParams.projectId, taskId: $routeParams.taskId}, $scope.comment, function() {
			$scope.comment = {};
			$scope.comments = Comments.query({projectId: $routeParams.projectId, taskId: $routeParams.taskId});
		}, function(errorResponse) {
			$scope.error = errorResponse;
		});
	};
}]);

dashboard.controller('UsersCtrl', ['$scope', '$routeParams', 'Users', function($scope, $routeParams, Users) {
	$scope.users = undefined;
	$scope.error = undefined;
	
	Users.query(function(users) {
		$scope.users = users;
	}, function(errorResult) {
		$scope.error = errorResult;
	});
}]);

dashboard.controller('UserCtrl', ['$scope', '$routeParams', '$location', 'Users', 'FileUploader', function($scope, $routeParams, $location, Users, FileUploader) {
	$scope.user = undefined;
	$scope.credentials = undefined;
	$scope.error = undefined;
	$scope.uploader = new FileUploader({method: 'PUT', url: '/api/avatars/' + $routeParams.userId, autoUpload: true, removeAfterUpload: true, alias: 'avatarFile'});
	
	Users.get({userId: $routeParams.userId}, function(user, responseHeaders) {
		$scope.user = user;
		$scope.pageTitle = user.givenName + ' ' + user.familyName;
	}, function(errorResult) {
		$scope.error = errorResult;
	});
	
	$scope.save = function() {
		$scope.user.$save(null, function(data, responseHeaders) {
			$location.path('/users/' + $scope.user.id);
		});
	};
}]);

dashboard.controller('UserCreateCtrl', ['$scope', '$location', 'Users', 'Credentials', function($scope, $location, Users, Credentials) {
	$scope.user = {};
	$scope.credentials = {
		userId:	undefined
	};
	$scope.pageTitle = 'Create user';
	$scope.error = undefined;
	
	$scope.save = function() {
		Users.save(null, $scope.user, function(data, responseHeaders) {
			$scope.credentials.userId = $scope.user.id;
			Credentials.save(null, $scope.credentials, function(data2, responseHeaders2) {
				$location.path('/users/' + $scope.user.id);
			});
		});
	};
}]);

dashboard.controller('UserRolesCtrl', ['$scope', '$routeParams', 'UserRoles', function($scope, $routeParams, UserRoles) {
	$scope.roles = UserRoles.query({userId: $routeParams.userId});
}]);

dashboard.controller('UserGroupsCtrl', ['$scope', '$routeParams', 'UserGroups', function($scope, $routeParams, UserGroups) {
	$scope.groups = UserGroups.query({userId: $routeParams.userId});
}]);

dashboard.controller('GroupsCtrl', ['$scope', '$routeParams', 'Groups', function($scope, $routeParams, Groups) {
	$scope.groups = undefined;
	$scope.error = undefined;
	
	Groups.query(function(groups) {
		$scope.groups = groups;
	}, function(errorResult) {
		$scope.error = errorResult;
	});
}]);

dashboard.controller('GroupCtrl', ['$scope', '$routeParams', '$location', 'Groups', function($scope, $routeParams, $location, Groups) {
	$scope.group = undefined;
	$scope.error = undefined;
	
	Groups.get({groupId: $routeParams.groupId}, function(group, responseHeaders) {
		$scope.group = group;
		$scope.pageTitle = group.name;
	}, function(errorResult) {
		$scope.error = errorResult;
	});
	
	$scope.save = function() {
		$scope.group.$save(null, function(data, responseHeaders) {
			$location.path('/groups/' + $scope.group.id);
		});
	};
}]);

dashboard.controller('GroupCreateCtrl', ['$scope', '$location', 'Groups', function($scope, $location, Groups) {
	$scope.group = {};
	$scope.pageTitle = 'Create group';
	$scope.error = undefined;
	
	$scope.save = function() {
		Groups.save(null, $scope.group, function(data, responseHeaders) {
			$location.path('/groups/' + $scope.group.id);
		});
	};
}]);

dashboard.controller('LoginCtrl', ['$scope', '$location', 'Auth', function($scope, $location, Auth) {
	$scope.credentials = {};
	$scope.error = undefined;
	
	$scope.login = function(credentials) {
		Auth.login(credentials).then(function() {
			$location.path('/');
		}, function(errorResponse) {
			$scope.credentials.password = undefined;
			$scope.error = errorResponse;
		});
	};
}]);

dashboard.controller('HeaderCtrl', ['$scope', 'Auth', '$location', function($scope, Auth, $location) {
	$scope.$watch(function() {
		return Auth.getUser();
	}, function(user) {
		$scope.user = user;
	}, true);
	
	$scope.logout = function() {
		Auth.logout();
		$scope.user = undefined;
		$location.path('/');
	};
}]);

dashboard.factory('Projects', ['$resource', function($resource) {
	return $resource('/api/projects/:projectId', {projectId: '@id'}, {save: {method: 'PUT'}});
}]);

dashboard.factory('Tasks', ['$resource', function($resource) {
	return $resource('/api/projects/:projectId/tasks/:taskId', {projectId: '@projectId', taskId: '@id'}, {save: {method: 'PUT'}});
}]);

dashboard.factory('Comments', ['$resource', function($resource) {
	return $resource('/api/projects/:projectId/tasks/:taskId/comments');
}]);

dashboard.factory('Users', ['$resource', function($resource) {
	return $resource('/api/users/:userId', {userId: '@id'}, {save: {method: 'PUT'}});
}]);

dashboard.factory('Credentials', ['$resource', function($resource) {
	return $resource('/api/users/:userId/credentials', {userId: '@userId'}, {save: {method: 'PUT'}});
}]);

dashboard.factory('UserTasks', ['$resource', function($resource) {
	return $resource('/api/users/:userId/tasks');
}]);

dashboard.factory('UserRoles', ['$resource', function($resource) {
	return $resource('/api/users/:userId/roles');
}]);

dashboard.factory('UserGroups', ['$resource', function($resource) {
	return $resource('/api/users/:userId/groups');
}]);

dashboard.factory('Groups', ['$resource', function($resource) {
	return $resource('/api/groups/:groupId', {groupId: '@id'}, {save: {method: 'PUT'}});
}]);

dashboard.factory('Auth', ['$http', '$q', '$cookies', 'Users', function($http, $q, $cookies, Users) {
	user = undefined;
	token = $cookies.get('XSRF-TOKEN');
	loginFailure = false;
	
	if (token) {
		Users.get({userId: 'authenticated'}, function(data, responseHeaders) {
			user = data;
		}, function(errorResult) {
			token = undefined;
			$cookies.remove('XSRF-TOKEN');
		});
	}
	
	return {
		login: function(credentials) {
			return $http.post('/api/auth/authenticate', credentials).
				success(function(data, status, headers, config) {
					token = data.token;
					user = Users.get({userId: 'authenticated'});
					loginFailure = false;
				}).
				error(function(data, status, headers, config) {
				});
		},
		logout: function() {
			$http.post('/api/auth/logout').
				success(function(data, status, headers, config) {
					$cookies.remove('XSRF-TOKEN');
					token = undefined;
					user = undefined;		
				});
			
		},
		getUser: function() {
			return user;
		}
	};
}]);

dashboard.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {
		templateUrl: '/assets/javascripts/templates/index.html',
		controller: 'HomeCtrl'
	})
	.when('/login', {
		templateUrl: '/assets/javascripts/templates/login.html',
		controller: 'LoginCtrl'
	})
	.when('/projects', {
		templateUrl: '/assets/javascripts/templates/projects/index.html',
		controller: 'ProjectsCtrl'
	})
	.when('/projects/create', {
		templateUrl: '/assets/javascripts/templates/projects/edit.html',
		controller: 'ProjectCreateCtrl'
	})
	.when('/projects/:projectId', {
		templateUrl: '/assets/javascripts/templates/projects/view.html',
		controller: 'ProjectCtrl'
	})
	.when('/projects/:projectId/edit', {
		templateUrl: '/assets/javascripts/templates/projects/edit.html',
		controller: 'ProjectCtrl'
	})
	.when('/projects/:projectId/tasks/:taskId', {
		templateUrl: '/assets/javascripts/templates/projects/task.html',
		controller: 'TaskCtrl'
	})
	.when('/users', {
		templateUrl: '/assets/javascripts/templates/users/index.html',
		controller: 'UsersCtrl'
	})
	.when('/users/create', {
		templateUrl: '/assets/javascripts/templates/users/edit.html',
		controller: 'UserCreateCtrl'
	})
	.when('/users/:userId', {
		templateUrl: '/assets/javascripts/templates/users/view.html',
		controller: 'UserCtrl'
	})
	.when('/users/:userId/edit' ,{
		templateUrl: '/assets/javascripts/templates/users/edit.html',
		controller: 'UserCtrl'
	})
	.when('/groups', {
		templateUrl: '/assets/javascripts/templates/groups/index.html',
		controller: 'GroupsCtrl'
	})
	.when('/groups/create', {
		templateUrl: '/assets/javascripts/templates/groups/edit.html',
		controller: 'GroupCreateCtrl'
	})
	.when('/groups/:groupId', {
		templateUrl: '/assets/javascripts/templates/groups/view.html',
		controller: 'GroupCtrl'
	})
	.when('/groups/:groupId/edit' ,{
		templateUrl: '/assets/javascripts/templates/groups/edit.html',
		controller: 'GroupCtrl'
	})
	.otherwise('/');
});
