# Dashboard schema

# --- !Ups

CREATE TABLE project (
  id VARCHAR(20) NOT NULL,
  name VARCHAR(128) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE user (
  id VARCHAR(20) NOT NULL,
  password VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  givenName VARCHAR(255) NOT NULL,
  familyName VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE task (
  id VARCHAR(20) NOT NULL,
  creator_id VARCHAR(20) NOT NULL,
  assignee_id VARCHAR(20),
  description VARCHAR(255) NOT NULL,
  project_id VARCHAR(20) NOT NULL,
  complete BIT(1) NOT NULL,
  FOREIGN KEY (creator_id) REFERENCES user(id),
  FOREIGN KEY (project_id) REFERENCES project(id),
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE taskdependency (
  task_id VARCHAR(20) NOT NULL,
  requiredTask_id VARCHAR(20) NOT NULL,
  FOREIGN KEY (task_id) REFERENCES task(id),
  FOREIGN KEY (requiredTask_id) REFERENCES task(id),
  PRIMARY KEY (task_id, requiredTask_id)
) ENGINE=InnoDB;

# --- !Downs

DROP TABLE IF EXISTS taskdependency;
DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS project;