# Groups, roles and permissions

# --- !Ups

CREATE TABLE `group` (
  id VARCHAR(20) NOT NULL,
  name VARCHAR(128) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE usergroup (
  user_id VARCHAR(20) NOT NULL,
  group_id VARCHAR(20) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(id),
  FOREIGN KEY (group_id) REFERENCES `group`(id),
  PRIMARY KEY (user_id, group_id)
) ENGINE=InnoDB;

CREATE TABLE role (
  id VARCHAR(20) NOT NULL,
  name VARCHAR(20) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE userrole (
  user_id VARCHAR(20) NOT NULL,
  role_id VARCHAR(20) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user(id),
  FOREIGN KEY (role_id) REFERENCES role(id),
  PRIMARY KEY (user_id, role_id)
) ENGINE=InnoDB;

CREATE TABLE grouprole (
  group_id VARCHAR(20) NOT NULL,
  role_id VARCHAR(20) NOT NULL,
  FOREIGN KEY (group_id) REFERENCES `group`(id),
  FOREIGN KEY (role_id) REFERENCES role(id),
  PRIMARY KEY (group_id, role_id)
) ENGINE=InnoDB;

CREATE TABLE permission (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  role_id VARCHAR(20) NOT NULL,
  target VARCHAR(255) NOT NULL,
  action VARCHAR(255) NOT NULL,
  granted BIT(1) NOT NULL,
  FOREIGN KEY (role_id) REFERENCES role(id),
  PRIMARY KEY (id)
) ENGINE=InnoDB;

# --- !Downs

DROP TABLE IF EXISTS `permission`;
DROP TABLE IF EXISTS `grouprole`;
DROP TABLE IF EXISTS `userrole`;
DROP TABLE IF EXISTS `role`;
DROP TABLE IF EXISTS `usergroup`;
DROP TABLE IF EXISTS `group`;
