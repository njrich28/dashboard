# Avatars

# --- !Ups

CREATE TABLE `avatar` (
  id VARCHAR(128) NOT NULL,
  mimeType VARCHAR(32) NOT NULL,
  imageData MEDIUMBLOB,
  PRIMARY KEY (id)
) ENGINE=InnoDB;


# --- !Downs

DROP TABLE IF EXISTS `avatar`;