# Task groups

# --- !Ups

ALTER TABLE `task` ADD COLUMN `assignedGroup_id` VARCHAR(20), ADD CONSTRAINT `fk_task_group` FOREIGN KEY (`assignedGroup_id`) REFERENCES `group`(`id`);
ALTER TABLE `task` DROP PRIMARY KEY, ADD PRIMARY KEY (`id`, `project_id`);

# --- !Downs

ALTER TABLE `task` DROP PRIMARY KEY, ADD PRIMARY KEY (`id`);
ALTER TABLE `task` DROP FOREIGN KEY `fk_task_group`, DROP COLUMN `assignedGroup_id`;