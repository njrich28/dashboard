# User credentials

# --- !Ups

ALTER TABLE `user` DROP COLUMN `password`;

CREATE TABLE `usercredentials` (
  `user_id` VARCHAR(20) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `user`(`id`),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB;

# --- !Downs

ALTER TABLE `user` ADD COLUMN `password` VARCHAR(255) NOT NULL;

DROP TABLE IF EXISTS `usercredentials`;
