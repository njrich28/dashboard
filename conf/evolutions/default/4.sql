# Comment

# --- !Ups

CREATE TABLE `comment` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `task_id` VARCHAR(20) NOT NULL,
  `creator_id` VARCHAR(20) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  FOREIGN KEY (task_id) REFERENCES task(id),
  FOREIGN KEY (creator_id) REFERENCES user(id),
  PRIMARY KEY (id)
) ENGINE=InnoDB;


# --- !Downs

DROP TABLE IF EXISTS `comment`;

