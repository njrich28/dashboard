package controllers

import org.scalatest._
import play.api.test._
import play.api.test.Helpers._
import org.scalatestplus.play._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject.bind
import play.api.Mode
import daos._
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers._
import scala.concurrent.Future
import models._
import play.api.mvc.Headers
import play.api.libs.json._
import play.api.http.MimeTypes
import com.google.inject.ImplementedBy
import play.api.inject.guice.GuiceableModule.fromPlayBinding
import play.api.libs.json.JsValue.jsValueToJsLookup
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import play.api.mvc.Result
import play.api.mvc.Cookie
import play.api.mvc.Request

class CommentApiSpec extends PlaySpec with OneAppPerSuite with MockitoSugar {
  
  val userId = "userId"
  val password = "p4ssword"
  val roleId = "roledId"
  val user = User(userId, "givenName", "familyName", "email")
  val userCredentials = UserCredentials(userId, password)
  val permissions = List(Permission(1, "roleId", "target", "action", true))
  val roles = List(Role(roleId, "name"))
  val xsrfToken = java.util.UUID.randomUUID().toString
  val identity = Identity(xsrfToken, user, roles, permissions)
  
  val taskId = "taskId"
  val comment1 = Comment(Some(1), taskId, userId, "Hello world!")
  val comment2 = Comment(Some(2), taskId, userId, "All your comment are belong to us!")
  
  def mockCommentDao = {
    val c = mock[CommentDao]
    
    // byTask
    when(c.byTask(anyString)) thenReturn Future.successful(Nil)
    when(c.byTask(taskId)) thenReturn Future.successful(List(comment1, comment2))
    c
  }
  
  def mockIdentityCache = {
    val c = mock[IdentityCache]
    when(c.get(anyString)) thenReturn None
    when(c.get(xsrfToken)) thenReturn Some(identity)
    c
  }
  
  def withSecurity[A](request: FakeRequest[A]): FakeRequest[A] = {
    request.withHeaders("X-XSRF-TOKEN" -> xsrfToken).withCookies(Cookie("XSRF-TOKEN", xsrfToken))
  }
  
  implicit override lazy val app = new GuiceApplicationBuilder()
    .in(Mode.Test)
    .overrides(bind[CommentDao].toInstance(mockCommentDao), bind[IdentityCache].toInstance(mockIdentityCache))
    .build
  
  "CommentApi" should {
    
    "return empty list if not found" in {
      val Some(result) = route(FakeRequest(GET, "/api/projects/projectId/tasks/notFound/comments"))
      status(result) must be(OK)
      contentType(result) mustEqual Some(MimeTypes.JSON)
      val body = contentAsJson(result)
      val maybeComments = body.validate[Array[JsValue]].asOpt
      maybeComments mustNot be(None)
      
      val comments = maybeComments.get
      comments.length must be(0)
    }
    
    "return comments if found" in {
      val Some(result) = route(FakeRequest(GET, "/api/projects/projectId/tasks/taskId/comments"))
      status(result) must be(OK)
      contentType(result) mustEqual Some(MimeTypes.JSON)
      val body = contentAsJson(result)
      val maybeComments = body.validate[Array[JsValue]].asOpt
      maybeComments mustNot be(None)
      
      val comments = maybeComments.get
      comments.length must be(2)
      
      (comments(0) \ "id").validate[Long].asOpt must be(Some(1))
      (comments(0) \ "creatorId").validate[String].asOpt must be(Some("userId"))
      (comments(0) \ "taskId").validate[String].asOpt must be(Some("taskId"))
      (comments(0) \ "value").validate[String].asOpt must be(Some("Hello world!"))
    }

  }
}