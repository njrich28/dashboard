
package controllers

import org.scalatest._
import play.api.test._
import play.api.test.Helpers._
import org.scalatestplus.play._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.inject.bind
import play.api.Mode
import daos._
import org.scalatest.mock.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers._
import scala.concurrent.Future
import models._
import play.api.mvc.Headers
import play.api.libs.json.Json
import play.api.http.MimeTypes
import com.google.inject.ImplementedBy
import play.api.inject.guice.GuiceableModule.fromPlayBinding
import play.api.libs.json.JsValue.jsValueToJsLookup
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import play.api.mvc.Result
import play.api.mvc.Cookie
import play.api.mvc.Request
import io.github.nremond.SecureHash

class UserApiSpec extends PlaySpec with OneAppPerSuite with MockitoSugar {
  
  val userId = "userId"
  val password = "p4ssword"
  val hashedPassword = SecureHash.createHash(password)
  val roleId = "roledId"
  val user = User(userId, "givenName", "familyName", "email")
  val userCredentials = UserCredentials(userId, hashedPassword)
  val permissions = List(Permission(1, "roleId", "target", "action", true))
  val roles = List(Role(roleId, "name"))
  val xsrfToken = java.util.UUID.randomUUID().toString
  val identity = Identity(xsrfToken, user, roles, permissions)
  
  
  def mockUserDao = {
    val u = mock[UserDao]
    
    // byId
    when(u.byId(anyString)) thenReturn Future.successful(None)
    when(u.byId(userId)) thenReturn Future.successful(Some(user))
    
    // all
    when(u.all) thenReturn Future.successful(List(user))
    // roles
    
    when(u.allRolesByUser(anyString)) thenReturn Future.successful(Nil)
    when(u.allRolesByUser(userId)) thenReturn Future.successful(roles)
    
    //permissions
    when(u.allPermissionsByUser(anyString)) thenReturn Future.successful(Nil)
    when(u.allPermissionsByUser(userId)) thenReturn Future.successful(permissions)
    
    // by credentials
    when(u.credentialsById(anyString)) thenReturn Future.successful(None)
    when(u.credentialsById(userId)) thenReturn Future.successful(Some(userCredentials))
    u
  }
  
  def mockIdentityCache = {
    val c = mock[IdentityCache]
    when(c.get(anyString)) thenReturn None
    when(c.get(xsrfToken)) thenReturn Some(identity)
    c
  }
  
  def withSecurity[A](request: FakeRequest[A]): FakeRequest[A] = {
    request.withHeaders("X-XSRF-TOKEN" -> xsrfToken).withCookies(Cookie("XSRF-TOKEN", xsrfToken))
  }
  
  implicit override lazy val app = new GuiceApplicationBuilder()
    .in(Mode.Test)
    .overrides(bind[UserDao].toInstance(mockUserDao), bind[IdentityCache].toInstance(mockIdentityCache))
    .build
  
  "UserApi" should {
    
    "not list users without authentication" in {
      val Some(result) = route(FakeRequest(GET, "/api/users"))
      status(result) must be(UNAUTHORIZED)
    }
    
    "list users with authentication" in {
      val Some(result) = route(withSecurity(FakeRequest(GET, "/api/users")))
      status(result) must be(OK)
    }
    
    "return 404 if user not found" in {
      val Some(result) = route(withSecurity(FakeRequest(GET, "/api/users/notfound")))
      status(result) must be(NOT_FOUND)
    }
    
    "not retrieve user without authentication" in {
      val Some(user) = route(FakeRequest(GET, s"/api/users/$userId"))
      status(user) must be(UNAUTHORIZED)
    }
    
    "retrieve user with authentication" in {
      val Some(user) = route(withSecurity(FakeRequest(GET, s"/api/users/$userId")))
      status(user) must be(OK)
    }
    
    "authenticate user with correct username and password" in {
      val Some(token) = route(FakeRequest(POST, "/api/auth/authenticate").withBody(Json.obj("username" -> userId, "password" -> password)))
      status(token) must be(OK)
      contentType(token) mustEqual Some(MimeTypes.JSON)
      val xsrfCookie = cookies(token).get("XSRF-TOKEN")
      xsrfCookie mustNot be(None)
      val xsrfTokenVal = xsrfCookie.get.value
      
      val body = contentAsJson(token)
      (body \ "status").validate[String].asOpt must be(Some("OK"))
      val bodyToken = (body \ "token").validate[String].asOpt 
      
      bodyToken mustNot be(None)
      bodyToken.get must be(xsrfTokenVal)
    }
    
    def shouldNotBeAuthed(token: Future[Result]) {
      status(token) must be(BAD_REQUEST)
      contentType(token) mustEqual Some(MimeTypes.JSON)
      val xsrfCookie = cookies(token).get("XSRF-TOKEN")
      xsrfCookie must be(None)
      
      val body = contentAsJson(token)     
      (body \ "status").validate[String].asOpt must be(Some("ERROR"))
      (body \ "message").validate[String].asOpt mustNot be(None)
      
    }
    
    "not authenticate user with incorrect password" in {
      val Some(token) = route(FakeRequest(POST, "/api/auth/authenticate").withBody(Json.obj("username" -> userId, "password" -> "wrongpassword")))
      shouldNotBeAuthed(token)
    }
    
    "not authenticate user with incorrect username" in {
      val Some(token) = route(FakeRequest(POST, "/api/auth/authenticate").withBody(Json.obj("username" -> "wrongUserId", "password" -> password)))
      shouldNotBeAuthed(token)
    }
    
    "not authenticate user with invalid JSON request" in {
      val Some(token) = route(FakeRequest(POST, "/api/auth/authenticate").withBody(Json.obj("username" -> userId)))
      shouldNotBeAuthed(token)
    }
  }
}