
import play.api.test._
import play.api.test.Helpers._
import org.scalatestplus.play._

class ApplicationSpec extends PlaySpec with OneAppPerSuite {

  "Application" should {

    "send 404 on a bad request" in {
      val Some(result) = route(FakeRequest(GET, "/boum"))
      status(result) mustEqual NOT_FOUND
    }

    "render the index page" in {
      val home = route(FakeRequest(GET, "/")).get

      status(home) must be(OK)
      contentType(home) must be(Some("text/html"))
//      contentAsString(home) must contain ("Your new application is ready.")
    }
  }
}
