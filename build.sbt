name := """dashboard"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  cache,
  ws,
  "org.webjars" % "bootstrap" % "3.3.5",
  "org.webjars" % "d3js" % "3.5.5-1",
  "org.webjars" % "angularjs" % "1.4.3",
  "org.webjars" % "nervgh-angular-file-upload" % "1.1.5-1",
  "com.typesafe.play" %% "play-slick" % "1.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "1.0.0",
  "mysql" % "mysql-connector-java" % "5.1.32",
  "io.github.nremond" %% "pbkdf2-scala" % "0.5",
  "org.scalatestplus" %% "play" % "1.4.0-M4" % "test",
  "org.mockito" % "mockito-core" % "1.10.19" % "test"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
